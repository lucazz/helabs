# -*- mode: ruby -*-
# vi: set ft=ruby :
Vagrant.configure(2) do |config|
  # Definindo qual vagrant box utilizar
  config.vm.box = "box-cutter/ubuntu1404-docker"
  # Habilitando o plugin hostmanager
  config.hostmanager.enabled = true
  # Habilitando a escrita no /etc/hosts da maquina HOST
  config.hostmanager.manage_host = true
  # Fazendo uso do endereço de IPv4 privado no alias helabs.dev
  config.hostmanager.ignore_private_ip = false
  # Desabilitando a inclusao de entradas para vms desligadas
  config.hostmanager.include_offline = false
  # Definindo endereço de IPv4 da interface de rede da VM via DHCPv4
  config.vm.define 'helabs' do |node|
    node.vm.hostname = 'helabs.dev'
    node.vm.network :private_network, type: "dhcp"
  end
  # Atribuindo mais RAM e vCPU's(VMWARE_FUSION) a VM
  config.vm.provider "vmware_fusion" do |v|
    v.gui = false
    v.vmx["memsize"] = "2048"
    v.vmx["numvcpus"] = "4"
  end
  # Atribuindo mais RAM e vCPU's(Virtualbox) a VM
  config.vm.provider "virtualbox" do |v|
    v.memory = 2048
    v.cpus = 4
  end
  # Enviando Dockerfile do worker (app[00-01]) para a VM
  config.vm.provision "file", source: "./docker/app", destination: "/home/vagrant/app"
  # Enviando Dockerfile do loadbalancer (nginx) para a VM
  config.vm.provision "file", source: "./docker/loadbalancer", destination: "/home/vagrant/loadbalancer"
  # Enviando docker-compose.yml para a VM
  config.vm.provision "file", source: "./docker-compose.yml", destination: "/home/vagrant/docker-compose.yml"
  # Enviando banco de dados sqlite da applicacao para a VM
  config.vm.provision "file", source: "./todo_list.db", destination: "/home/vagrant/todo_list.db"
  # Executando o docker-compose up
  config.vm.provision :docker_compose, yml: "/home/vagrant/docker-compose.yml", run: "always"
end
