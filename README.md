# Processo Seletivo HE:labs - DevOps

Participante: [Lucas do Amaral Saboya](https://twitter.com/lucazz)

Este repositório é a resolução do [exercício](https://gist.github.com/helabstech/befa484a1f2f07d41fd5)
para a entrevista de DevOps na HE:Labs!

## Das tecnologias adotadas

Decidi fazer uma única virtual com 3 containers fazendo uso de:

*   [Vagrant](https://www.vagrantup.com)
*   [Boxcutter vagrant boxes](https://vagrantcloud.com/box-cutter)
*   [Plugin para o Vagrant: hostmanager](https://github.com/smdahlen/vagrant-hostmanager)
*   [Plugin para o Vagrant: vagrant-docker-compose](https://github.com/leighmcculloch/vagrant-docker-compose)
*   [Docker](http://www.docker.com/)

O Vagrant irá provisionar uma máquina virtual com Docker instalado
(Box utilizada: box-cutter/ubuntu1404-docker), instalará o docker-compose e em
seguida construirá e levantará os containers tendo os fontes baixados do
[repositório](https://github.com/timmillwood/todo_list), como descrito no [exercício](https://gist.github.com/helabstech/befa484a1f2f07d41fd5).

## Pre-requisitos

Para que a aplicação seja executada, preciso que você instale:

*   [Vagrant](https://www.vagrantup.com)
*   [Plugin do Vagrant: hostmanager](https://github.com/smdahlen/vagrant-hostmanager)
*   [Plugin do Vagrant: vagrant-docker-compose](https://github.com/leighmcculloch/vagrant-docker-compose)

## Executando

Para que a aplicação seja executada, apenas digite:

```
$ vagrant plugin install vagrant-docker-compose
$ vagrant plugin install vagrant-hostmanager
$ git clone https://lucazz@bitbucket.org/lucazz/helabs.git .
$ vagrant up
```

## Acessando nossa app

Para evitar que tenhamos de executar o vagrant com privilegios de superusuario,
mapeei a porta da nossa app para "8080", logo, para acessa-la basta apontar seu
browser para a seguinte URL:

```
http://helabs.dev:8080/
```

## Referencias

*   [Plugin do Vagrant: hostmanager](https://github.com/smdahlen/vagrant-hostmanager)
*   [Plugin do Vagrant: vagrant-docker-compose](https://github.com/leighmcculloch/vagrant-docker-compose)
*   [Pagina de referencia do Dockerfile](http://docs.docker.com/engine/reference/builder/)
*   [Pagina de referencia do Docker-compose](http://docs.docker.com/compose/compose-file/)
*   [Boxcutter: vagrant boxes "pre-baked"](https://github.com/boxcutter)